/* eslint-disable */
import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/Home'
import Book from '@/components/Book'
import Book_Type from '@/components/Book_Type'

Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'HelloWorld',
    //   component: HelloWorld
    // },
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/book/:page',
        name: 'Book',
        component: Book,
    },
    {
      path: '/booktype',
      name: 'Book_Type',
      component: Book_Type,
    }
  ],
    mode: 'history',
})
